# OpsGenieMini

OpsGenie uygulamasının 2 ekranlı sample uygulaması.

## Push Detayları


### Rest Test Client ile push atma

RESTApi test client kullanarak (Postman ya da Restlet) aşağıdaki request ile push atılabilir

```
Content-Type:application/json
Authorization:key=AAAAigfzRcQ:APA91bGEbT4OgYo4GQyGb9JisyZbmAu6oIhWXy7joWm8Ti32wxysZVUIZqconNgUlQhESGJNQ-tK_FF58x1-w3D6K32ofUCW50-Ndv5gkswlQPOMhD3QrccKsBzSMA98W4gLsW2QznXx

{
    "priority" : "high",
    "time_to_live" : 0,
    "to": "[FCM DEVICE_TOKEN]",
    "data" : {
        "alert_msg" :"testMessage",
        "alert_id":"68af9a4e-2c55-4942-bb2f-5c9ab7a9922e-1527245153120"
    }
}
```

### cUrl ile push atma


```
curl -X POST
-H 'Authorization:key=AAAAigfzRcQ:APA91bGEbT4OgYo4GQyGb9JisyZbmAu6oIhWXy7joWm8Ti32wxysZVUIZqconNgUlQhESGJNQ-tK_FF58x1-w3D6K32ofUCW50-Ndv5gkswlQPOMhD3QrccKsBzSMA98W4gLsW2QznXx'
-H 'Content-type:application/json'
-d '{"priority":"high","to":"[FCM_DEVICE_TOKEN]","data":{"alert_msg" :"testMessage","alert_id":"68af9a4e-2c55-4942-bb2f-5c9ab7a9922e-1527245153120"}}'
'https://fcm.googleapis.com/fcm/send'
```