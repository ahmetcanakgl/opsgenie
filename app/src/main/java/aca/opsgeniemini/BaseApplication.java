package aca.opsgeniemini;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class BaseApplication extends Application {


    private static Context context;
    private static Typeface regularTypeface = null;
    private static Typeface boldTypeface = null;

    public static Context getAppContext() {
        return BaseApplication.context;
    }

    public static Typeface getRegularTypeFace() {
        if (regularTypeface == null) {
            regularTypeface = Typeface.createFromAsset(getAppContext().getAssets(), "fonts/Gudea_Regular.ttf");
        }
        return regularTypeface;
    }

    public static Typeface getBoldTypeFace() {
        if (boldTypeface == null) {
            boldTypeface = Typeface.createFromAsset(getAppContext().getAssets(), "fonts/Gudea_Bold.ttf");
        }
        return boldTypeface;
    }

    public static String getElapsedTime(String date) {

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.GERMANY);
        long milliseconds = 0;
        long milisDifference;
        int addition = 0;
        try {
            Date d = f.parse(date);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        milisDifference = System.currentTimeMillis() - milliseconds;

        if (milisDifference < 60000) {
            return String.valueOf((int) (milisDifference / 1000) + addition) + context.getString(R.string.second);
        } else if (milisDifference >= 60000 && milisDifference < 3600000) {
            return String.valueOf((int) (milisDifference / 60000) + addition) + context.getString(R.string.minute);
        } else if (milisDifference >= 3600000 && milisDifference < 86400000) {
            return String.valueOf((int) (milisDifference / 3600000) + addition) + context.getString(R.string.hour);
        } else if (milisDifference >= 86400000 && milisDifference < 604800000) {
            return String.valueOf((int) (milisDifference / 86400000) + addition) + context.getString(R.string.day);
        } else if (milisDifference >= 604800000 && milisDifference < 2592000000L) {
            return String.valueOf((int) (milisDifference / 604800000) + addition) + context.getString(R.string.week);
        } else if (milisDifference >= 2592000000L && milisDifference < 31104000000L) {
            return String.valueOf((int) (milisDifference / 2592000000L) + addition) + context.getString(R.string.month);
        } else
            return String.valueOf((int) (milisDifference / 31104000000L) + addition) + context.getString(R.string.year);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

}