package aca.opsgeniemini.util;

import android.content.Context;
import android.util.AttributeSet;

import aca.opsgeniemini.BaseApplication;

//custom textView for titles
public class CTextView extends android.support.v7.widget.AppCompatTextView {

    public CTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            setTypeface(BaseApplication.getBoldTypeFace());
        }
    }
}
