package aca.opsgeniemini.util;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import aca.opsgeniemini.Constants;
import aca.opsgeniemini.model.AckModel;
import aca.opsgeniemini.model.POSTmodel.POSTAck;
import aca.opsgeniemini.web.ApiCall;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AckReceiver extends BroadcastReceiver {

    Context ctx;

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        ctx = context;
        String alertId = intent.getStringExtra(Constants.ALERT_ID);
        new ApiCall().postAckCall(alertId, new POSTAck("TestUser", "TestSource", "TestNote"), new Callback<AckModel>() {
            @Override
            public void onResponse(Call<AckModel> call, Response<AckModel> response) {
                Log.d("ACKSuccess", response.body().getResult());
                Toast.makeText(ctx, response.body().getResult(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<AckModel> call, Throwable t) {
                Log.e("ACKError", t.getMessage());
                Toast.makeText(ctx, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
