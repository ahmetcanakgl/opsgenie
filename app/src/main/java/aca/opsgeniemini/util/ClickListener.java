package aca.opsgeniemini.util;

import android.view.View;

/**
 * Created by AHMET CAN AKGUL on 3.07.2016.
 */
public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
