package aca.opsgeniemini.util;

import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.ViewGroup;
import android.widget.TextView;

import aca.opsgeniemini.BaseApplication;
import aca.opsgeniemini.Constants;
import aca.opsgeniemini.R;

/**
 * Created by AHMET CAN AKGUL on 10.p09.2016.
 */
public class SnackBar {

    public static void showSnack(String message, CoordinatorLayout snack, String command) {

        Snackbar snackbar = Snackbar.make(snack, message, Snackbar.LENGTH_LONG);
        ViewGroup group = (ViewGroup) snackbar.getView();
        if (command.equals(Constants.ERROR)) {
            group.setBackgroundColor(BaseApplication.getAppContext().getResources().getColor(R.color.snackbar_fail_background));
            snackbar.setActionTextColor(BaseApplication.getAppContext().getResources().getColor(R.color.snackbar_fail_action_text));
        } else if (command.equals(Constants.SUCCESS)) {
            group.setBackgroundColor(BaseApplication.getAppContext().getResources().getColor(R.color.snackbar_success_background));
            snackbar.setActionTextColor(BaseApplication.getAppContext().getResources().getColor(R.color.snackbar_success_action_text));
        }
        TextView textView = group.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(15);
        snackbar.show();

    }

    public static void showErrorMessage(@Nullable String responseBodyError, CoordinatorLayout snack) {

        if (responseBodyError == null) {
            responseBodyError = BaseApplication.getAppContext().getString(R.string.unknown_error);
        }

        SnackBar.showSnack(responseBodyError,
                snack,
                Constants.ERROR);
    }

    public static void showSuccessMessage(@Nullable String successMessage, CoordinatorLayout snack) {

        if (successMessage == null) {
            successMessage = BaseApplication.getAppContext().getString(R.string.success);
        }

        SnackBar.showSnack(successMessage,
                snack,
                Constants.SUCCESS);
    }

}