package aca.opsgeniemini.util;

import android.app.ProgressDialog;
import android.content.Context;

import aca.opsgeniemini.R;

/**
 * Created by AHMET CAN AKGUL on 30.09.2016.
 */

public class ProgressDialogUtil {

    public ProgressDialog mProgressDialog;

    public ProgressDialogUtil(Context context) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(context.getString(R.string.loading));
        mProgressDialog.setCancelable(false);
    }

    public ProgressDialogUtil(Context context, String message) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(true);
    }

    public void showProgress() {
        mProgressDialog.show();
    }

    public void hideProgress() {
        if (mProgressDialog.isShowing()) mProgressDialog.dismiss();
    }
}
