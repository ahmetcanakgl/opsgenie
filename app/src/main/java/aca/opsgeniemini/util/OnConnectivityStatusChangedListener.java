package aca.opsgeniemini.util;

/**
 * Created by AHMET CAN AKGUL on 21.06.2016.
 */
public interface OnConnectivityStatusChangedListener {
    void isOnline(boolean isOnline);
}
