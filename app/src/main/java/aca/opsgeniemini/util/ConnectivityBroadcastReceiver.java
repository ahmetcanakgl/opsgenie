package aca.opsgeniemini.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

/**
 * Created by AHMET CAN AKGUL on 4.07.2016.
 */
public class ConnectivityBroadcastReceiver extends BroadcastReceiver {

    // Reference to OnConnectivityTypeChangedListener interface.
    private OnConnectivityStatusChangedListener connectivityTypeChangedListener = null;

    @Override
    public void onReceive(Context context, Intent intent) {

        // Set OnConnectivityTypeChangedListener interface.
        setOnConnectivityTypeChangedListener(context);

        if (connectivityTypeChangedListener != null) {
            // Call interface's methods. So results can be useable in parent Activity.
            connectivityTypeChangedListener.isOnline(isDeviceOnline(context));
        }
    }

    public void setOnConnectivityTypeChangedListener(Context context) {
        try {
            this.connectivityTypeChangedListener = (OnConnectivityStatusChangedListener) context;
        } catch (ClassCastException castException) {
            Log.d("", "ConnectivityBroadcastReceiver - setOnConnectivityTypeChangedListener : error " + castException.toString());
        }
    }

    public boolean isDeviceOnline(Context _context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);

        // Return true if and only if there is an active network and this network's current status is "connected".
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
