package aca.opsgeniemini.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import aca.opsgeniemini.BaseApplication;
import aca.opsgeniemini.R;
import aca.opsgeniemini.model.submodel.Alert;

public class AlertAdapter extends RecyclerView.Adapter<AlertAdapter.mViewHolder> {

    private Context context;
    private ArrayList<Alert> alertArrayList;

    public AlertAdapter(ArrayList<Alert> list, Context context) {
        this.alertArrayList = list;
        this.context = context;
    }

    public AlertAdapter.mViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_alert, parent, false);
        return new AlertAdapter.mViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AlertAdapter.mViewHolder holder, final int position) {

        final Alert alertModel = alertArrayList.get(position);

        holder.message.setText(alertModel.getMessage());
        holder.message.setTextColor(context.getResources().getColor(R.color.black));
        holder.message.setTypeface(BaseApplication.getRegularTypeFace());

        holder.owner.setText(alertModel.getOwner().equals("") ? context.getString(R.string.no_owner) : alertModel.getOwner());
        holder.owner.setTextColor(context.getResources().getColor(R.color.black));
        holder.owner.setTypeface(BaseApplication.getRegularTypeFace());

        if (alertModel.getAcknowledged()) {
            holder.ackStatusText.setText(context.getString(R.string.acked));
            holder.ackStatusText.setTextColor(context.getResources().getColor(R.color.cyan));
            holder.ackIcon.setBackground(context.getResources().getDrawable(R.drawable.selected_icon));
        } else {
            holder.ackStatusText.setText(context.getString(R.string.not_acked));
            holder.ackStatusText.setTextColor(context.getResources().getColor(R.color.grey_light));
            holder.ackIcon.setBackground(context.getResources().getDrawable(R.drawable.unselected_icon));
        }
        holder.ackStatusText.setTypeface(BaseApplication.getRegularTypeFace());

        holder.elapsedTimeText.setText(convertSpecificDate(alertModel.getCreatedAt()));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return alertArrayList.size();
    }

    private String convertSpecificDate(String date) {
        return BaseApplication.getElapsedTime(date);
    }

    class mViewHolder extends RecyclerView.ViewHolder {

        TextView message, owner, ackStatusText, elapsedTimeText;
        ImageView ackIcon;

        mViewHolder(View view) {
            super(view);
            message = view.findViewById(R.id.message);
            owner = view.findViewById(R.id.owner);
            ackStatusText = view.findViewById(R.id.ack_status_text);
            elapsedTimeText = view.findViewById(R.id.elapsed_time_text);
            ackIcon = view.findViewById(R.id.ack_icon);
        }
    }
}
