package aca.opsgeniemini.push;

/**
 * Created by ahmetcanakgl on 07-Jul-17.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import aca.opsgeniemini.Constants;
import aca.opsgeniemini.R;
import aca.opsgeniemini.controller.activity.MainActivity;
import aca.opsgeniemini.model.FirebaseNotificationModel;
import aca.opsgeniemini.util.AckReceiver;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData().size() > 0) {

            scheduleJob();

            FirebaseNotificationModel fcmModel = new FirebaseNotificationModel();
            JSONObject obj = new JSONObject(remoteMessage.getData());
            try {
                fcmModel.setAlertMessage(obj.getString("alert_msg"));
                fcmModel.setAlertId(obj.getString("alert_id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            sendNotification(fcmModel);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            System.out.println("!!!!!Message Notifications Body: " + remoteMessage.getNotification().getBody());
        }
    }

    /**
     * No need for OpsGenie
     */
    private void scheduleJob() {

        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("tag")
                .build();
        dispatcher.schedule(myJob);
    }

    private void sendNotification(FirebaseNotificationModel fcmModel) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.ALERT_ID, fcmModel.getAlertId());

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        Intent ackIntent = new Intent(this, AckReceiver.class);
        ackIntent.putExtra(Constants.ALERT_ID, fcmModel.getAlertId());
        PendingIntent ackPendingIntent =
                PendingIntent.getBroadcast(this, 0, ackIntent, 0);


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("New Alert !")
                .setContentText(fcmModel.getAlertMessage())
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .addAction(R.drawable.selected_icon, getString(R.string.ack), ackPendingIntent);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder.setSound(defaultSoundUri);

        long[] pattern = {0, 500, 1000};
//        terminator
//        long[] pattern = {0,44,101,63,242,62,244,61,117,51,915,83,86,62,264,62,267,49,
//                            127,64,749,64,128,85,239,420,172,143,213,104,264,73,0};
//        enter sandman
//        long[] pattern = {0,55,171,54,175,56,160,69,178,52,170,53,170,63,175,
//                            45,85,29,86,54,186,64,203,62,187,51,195,61,191,49,189,63,182,
//                            38,111,37,102,52,189,46,202,44,209,56,187,60,187,42,193,54,179,
//                            56,94,50,112,33,201,46,174,55,177,283,194,304,178,93,171,110,
//                            131,65,168,64,175,63,184,62,186,56,182,58,179,57,71,59,98,60,
//                            185,47,203,40,192,38,201,54,220,52,179,40,203,40,96,53,105,65,
//                            185,50,207,53,227,45,200,39,203,53,182,47,180,372,111,90,165,
//                            111,130,57,165,82,149,103,150,88,159,150,0};
        notificationBuilder.setVibrate(pattern);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int id = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

        if (notificationManager != null) {
            notificationManager.notify(id, notificationBuilder.build());
        }
    }
}