package aca.opsgeniemini;

public class Constants {

    public static final int PAGE_SIZE = 20;
    public static final String ERROR = "ERROR";
    public static final String SUCCESS = "SUCCESS";
    public static final String ALERT_ID = "ALERT_ID";
}
