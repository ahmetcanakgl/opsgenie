package aca.opsgeniemini.controller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import aca.opsgeniemini.Constants;
import aca.opsgeniemini.R;
import aca.opsgeniemini.model.AlertDetailModel;
import aca.opsgeniemini.model.submodel.Alert;
import aca.opsgeniemini.model.submodel.Team;
import aca.opsgeniemini.util.ProgressDialogUtil;
import aca.opsgeniemini.web.ApiCall;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AlertDetailFragment extends Fragment {

    private Context context;
    private SwipeRefreshLayout swipeContainer;
    private TextView msgTextV, createdAtTextV, statusTextV,
            priorityTextV, ownerTextV, countTextV, sourceTextV,
            integrationTextV, teamsTextV, descTextV, linkTextV,
            tagsTextV, tinyIdTextV, entityTextV, aliasTextV, updateAtTextV;
    private ProgressDialogUtil progressDialogUtil;

    private AlertDetailModel alertDetailModel;
    private String alertId;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_alert_detail, container, false);
        context = getActivity();

        Bundle bundle = getArguments();
        alertId = bundle.getString(Constants.ALERT_ID);

        setUI(layout);
        setListeners();

        if (alertId != null) {
            getAlertDetail();
        } else {
            Toast.makeText(context, "ID Error.", Toast.LENGTH_SHORT).show();
        }

        return layout;
    }

    private void setUI(View v) {
        progressDialogUtil = new ProgressDialogUtil(context);
        swipeContainer      = v.findViewById(R.id.swipeContainer);

        msgTextV            = v.findViewById(R.id.msg);
        createdAtTextV      = v.findViewById(R.id.created_at);
        statusTextV         = v.findViewById(R.id.status);
        priorityTextV       = v.findViewById(R.id.priority);
        ownerTextV          = v.findViewById(R.id.owner);
        countTextV          = v.findViewById(R.id.count);
        sourceTextV         = v.findViewById(R.id.source);
        integrationTextV    = v.findViewById(R.id.integration);
        teamsTextV          = v.findViewById(R.id.teams);
        descTextV           = v.findViewById(R.id.desc);
        linkTextV           = v.findViewById(R.id.link);
        tagsTextV           = v.findViewById(R.id.tags);
        tinyIdTextV         = v.findViewById(R.id.tiny_id);
        entityTextV         = v.findViewById(R.id.entity);
        aliasTextV          = v.findViewById(R.id.alias);
        updateAtTextV       = v.findViewById(R.id.update_at);
    }

    private void setListeners() {
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });
    }

    private void refreshData() {

        alertDetailModel = null;
        getAlertDetail();
    }

    private void getAlertDetail() {

        if (!swipeContainer.isRefreshing()) {
            progressDialogUtil.showProgress();
        }

        new ApiCall().getAlertDetailCall(alertId, new Callback<AlertDetailModel>() {
            @Override
            public void onResponse(Call<AlertDetailModel> call, Response<AlertDetailModel> response) {
                if (response.isSuccessful()) {
                    alertDetailModel = response.body();
                    fillUI();
                }
            }

            @Override
            public void onFailure(Call<AlertDetailModel> call, Throwable t) {
                swipeContainer.setRefreshing(false);
                progressDialogUtil.hideProgress();
            }
        });
    }

    private void fillUI() {

        final Alert mAlert = alertDetailModel.getAlert();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                String teamStr = "";
                String tagStr = "";

                if (mAlert.getTeams().size() != 0) {
                    for (Team team : mAlert.getTeams()) {
                        teamStr = teamStr.concat(team.getId());
                    }
                }

                if (mAlert.getTags().size() != 0) {
                    for (String tag : mAlert.getTags()) {
                        tagStr = teamStr.concat(tag);
                    }
                }

                msgTextV.setText(mAlert.getMessage());
                createdAtTextV.setText(mAlert.getCreatedAt());
                statusTextV.setText(mAlert.getStatus());
                priorityTextV.setText(mAlert.getPriority());
                ownerTextV.setText(mAlert.getOwner());
                countTextV.setText(mAlert.getCount());
                sourceTextV.setText(mAlert.getSource());
                integrationTextV.setText(mAlert.getIntegration().getName());
                teamsTextV.setText(teamStr);
                descTextV.setText(mAlert.getDescription());
                linkTextV.setText(mAlert.getDetails().getLink());
                tagsTextV.setText(tagStr);
                tinyIdTextV.setText(mAlert.getTinyId());
                entityTextV.setText(mAlert.getEntity());
                aliasTextV.setText(mAlert.getAlias());
                updateAtTextV.setText(mAlert.getUpdatedAt());

                swipeContainer.setRefreshing(false);
                progressDialogUtil.hideProgress();
            }
        });
    }

}
