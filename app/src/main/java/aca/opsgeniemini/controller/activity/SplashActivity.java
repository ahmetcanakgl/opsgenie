package aca.opsgeniemini.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;

import aca.opsgeniemini.R;
import aca.opsgeniemini.util.ConnectivityBroadcastReceiver;
import aca.opsgeniemini.util.OnConnectivityStatusChangedListener;
import aca.opsgeniemini.util.SnackBar;

public class SplashActivity extends AppCompatActivity implements OnConnectivityStatusChangedListener {

    public static final int SPLASH_TIME_OUT = 1000;
    ConnectivityBroadcastReceiver ICBroadcastReceiver = new ConnectivityBroadcastReceiver();
    Boolean internetIsConnected = true;
    Context context;
    CoordinatorLayout snack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        context = this;
        snack = findViewById(R.id.snackbarlocation);

        if (internetIsConnected)
            waitForTimeout();
    }

    private void openNextActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void waitForTimeout() {

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                openNextActivity();
            }
        }, SPLASH_TIME_OUT);
    }


    @Override
    public void isOnline(boolean isOnline) {
        if (!isOnline) {
            SnackBar.showErrorMessage(getString(R.string.no_connection), snack);
            internetIsConnected = false;
        }

        if (isOnline && !internetIsConnected) {
            SnackBar.showSuccessMessage(getString(R.string.connection_success), snack);
            internetIsConnected = true;
            waitForTimeout();
        }
    }

    @Override
    public void onResume() {
        IntentFilter connectivityIntentFilter = new IntentFilter();
        connectivityIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(ICBroadcastReceiver, connectivityIntentFilter);
        super.onResume();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(ICBroadcastReceiver);
        super.onPause();
    }
}