package aca.opsgeniemini.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import aca.opsgeniemini.Constants;
import aca.opsgeniemini.R;
import aca.opsgeniemini.adapter.AlertAdapter;
import aca.opsgeniemini.controller.fragment.AlertDetailFragment;
import aca.opsgeniemini.model.AlertModel;
import aca.opsgeniemini.model.submodel.Alert;
import aca.opsgeniemini.util.ClickListener;
import aca.opsgeniemini.util.ConnectivityBroadcastReceiver;
import aca.opsgeniemini.util.DividerItemDecoration;
import aca.opsgeniemini.util.EndlessRecyclerViewScrollListener;
import aca.opsgeniemini.util.OnConnectivityStatusChangedListener;
import aca.opsgeniemini.util.ProgressDialogUtil;
import aca.opsgeniemini.util.RecyclerTouchListener;
import aca.opsgeniemini.util.SnackBar;
import aca.opsgeniemini.web.ApiCall;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements OnConnectivityStatusChangedListener {

    private ConnectivityBroadcastReceiver ICBroadcastReceiver = new ConnectivityBroadcastReceiver();
    private Boolean internetIsConnected = true;
    private CoordinatorLayout snack;
    private Context context;
    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private SwipeRefreshLayout swipeContainer;
    private ProgressDialogUtil progressDialogUtil;
    private AlertAdapter alertAdapter;

    private RelativeLayout fragContainer;

    private ArrayList<Alert> alertArrayList;
    private ArrayList<Alert> alertArrayListTotal = new ArrayList<>();

    private int offset = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        Intent intent = getIntent();
        String alertIdPush = intent.getStringExtra(Constants.ALERT_ID);

        setUI();
        setListeners();

        if (alertIdPush == null) {   //regular app progress (no push is received yet)
            getAlerts();
        } else {                    //push with alertId is received.
            openAlertDetail(alertIdPush);
        }
    }

    private void setUI() {
        progressDialogUtil = new ProgressDialogUtil(context);
        mLayoutManager = new LinearLayoutManager(context);
        fragContainer = findViewById(R.id.frag_container);
        swipeContainer = findViewById(R.id.swipeContainer);
        snack = findViewById(R.id.snackbarlocation);

        recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getResources()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void setListeners() {

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (alertArrayList.size() == Constants.PAGE_SIZE) {
                    reloadWithOffset();
                }
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                openAlertDetail(alertArrayListTotal.get(position).getId());
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });
    }

    private void openAlertDetail(String alertId) {
        FragmentManager fM = getSupportFragmentManager();
        FragmentTransaction fT = fM.beginTransaction();
        AlertDetailFragment alertDetailFragment = new AlertDetailFragment();
        String title = "alert_detail";

        Bundle bundle = new Bundle();
        bundle.putString(Constants.ALERT_ID, alertId);

        alertDetailFragment.setArguments(bundle);
        fT.addToBackStack(title);
        fT.replace(R.id.frag_container, alertDetailFragment, title);
        fT.commit();
        showFragContainer();
    }

    private void showFragContainer() {
        fragContainer.setVisibility(View.VISIBLE);
    }

    private void hideFragContainer() {
        fragContainer.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count != 0) {
            getSupportFragmentManager().popBackStack();
            if (count == 1) {
                hideFragContainer();
                refreshData();
            }
        } else {
            super.onBackPressed();
        }
    }

    private void refreshData() {

        alertArrayListTotal.clear();
        if (alertAdapter != null)
            alertAdapter.notifyDataSetChanged();
        offset = 0;
        getAlerts();
    }

    private void getAlerts() {

        if (!swipeContainer.isRefreshing()) {
            progressDialogUtil.showProgress();
        }

        new ApiCall().getAlertsCall(offset, new Callback<AlertModel>() {
            @Override
            public void onResponse(Call<AlertModel> call, Response<AlertModel> response) {
                if (response.isSuccessful()) {
                    alertArrayList = response.body().getAlertArrayList();
                    alertArrayListTotal.addAll(alertArrayList);

                    if (alertAdapter == null) {
                        alertAdapter = new AlertAdapter(alertArrayListTotal, context);
                        recyclerView.setAdapter(alertAdapter);
                        System.out.println("!!!!!!!!!! ADAPTER was NULL");
                    } else {
                        alertAdapter.notifyDataSetChanged();
                        System.out.println("!!!!!!!!!! ADAPTER was NOT NULL");
                    }
                }
                swipeContainer.setRefreshing(false);
                progressDialogUtil.hideProgress();
            }

            @Override
            public void onFailure(Call<AlertModel> call, Throwable t) {
                swipeContainer.setRefreshing(false);
                progressDialogUtil.hideProgress();
            }
        });
    }

    // reload page with updated offset
    public void reloadWithOffset() {
        offset += 20;
        getAlerts();
    }

    @Override
    public void isOnline(boolean isOnline) {
        if (!isOnline) {
            SnackBar.showErrorMessage(getString(R.string.no_connection), snack);
            internetIsConnected = false;
        }

        if (isOnline && !internetIsConnected) {
            SnackBar.showSuccessMessage(getString(R.string.connection_success), snack);
            internetIsConnected = true;
            refreshData();
        }
    }

    @Override
    public void onResume() {

        IntentFilter connectivityIntentFilter = new IntentFilter();
        connectivityIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(ICBroadcastReceiver, connectivityIntentFilter);
        refreshData();
        super.onResume();
    }

    @Override
    protected void onPause() {

        unregisterReceiver(ICBroadcastReceiver);
        super.onPause();
    }

    public void onBackPressed(View view) {
        onBackPressed();
    }
}
