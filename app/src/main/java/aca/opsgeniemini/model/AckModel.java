package aca.opsgeniemini.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AckModel {

    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("took")
    @Expose
    private String took;
    @SerializedName("requestId")
    @Expose
    private String requestId;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTook() {
        return took;
    }

    public void setTook(String took) {
        this.took = took;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
