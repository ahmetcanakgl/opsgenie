package aca.opsgeniemini.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import aca.opsgeniemini.model.submodel.Alert;
import aca.opsgeniemini.model.submodel.Paging;

public class AlertModel {

    @SerializedName("data")
    @Expose
    private ArrayList<Alert> alertArrayList = null;
    @SerializedName("paging")
    @Expose
    private Paging paging;
    @SerializedName("took")
    @Expose
    private String took;
    @SerializedName("requestId")
    @Expose
    private String requestId;

    public ArrayList<Alert> getAlertArrayList() {
        return alertArrayList;
    }

    public void setAlertArrayList(ArrayList<Alert> alertArrayList) {
        this.alertArrayList = alertArrayList;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public String getTook() {
        return took;
    }

    public void setTook(String took) {
        this.took = took;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
