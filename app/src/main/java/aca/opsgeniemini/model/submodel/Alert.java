package aca.opsgeniemini.model.submodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Alert {

    @SerializedName("seen")
    @Expose
    private Boolean seen;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("tinyId")
    @Expose
    private String tinyId;
    @SerializedName("alias")
    @Expose
    private String alias;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("acknowledged")
    @Expose
    private Boolean acknowledged;
    @SerializedName("isSeen")
    @Expose
    private Boolean isSeen;
    @SerializedName("tags")
    @Expose
    private ArrayList<String> tags = null;
    @SerializedName("snoozed")
    @Expose
    private Boolean snoozed;
    @SerializedName("count")
    @Expose
    private String count;
    @SerializedName("lastOccurredAt")
    @Expose
    private String lastOccurredAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("teams")
    @Expose
    private ArrayList<Team> teams = null;
    @SerializedName("responders")
    @Expose
    private ArrayList<Responder> responders = null;
    @SerializedName("integration")
    @Expose
    private Integration integration;
    @SerializedName("actions")
    @Expose
    private ArrayList<Object> actions = null;
    @SerializedName("report")
    @Expose
    private Report report;
    @SerializedName("entity")
    @Expose
    private String entity;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("details")
    @Expose
    private Details details;

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTinyId() {
        return tinyId;
    }

    public void setTinyId(String tinyId) {
        this.tinyId = tinyId;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(Boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    public Boolean getIsSeen() {
        return isSeen;
    }

    public void setIsSeen(Boolean isSeen) {
        this.isSeen = isSeen;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public Boolean getSnoozed() {
        return snoozed;
    }

    public void setSnoozed(Boolean snoozed) {
        this.snoozed = snoozed;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getLastOccurredAt() {
        return lastOccurredAt;
    }

    public void setLastOccurredAt(String lastOccurredAt) {
        this.lastOccurredAt = lastOccurredAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<Team> teams) {
        this.teams = teams;
    }

    public ArrayList<Responder> getResponders() {
        return responders;
    }

    public void setResponders(ArrayList<Responder> responders) {
        this.responders = responders;
    }

    public Integration getIntegration() {
        return integration;
    }

    public void setIntegration(Integration integration) {
        this.integration = integration;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public ArrayList<Object> getActions() {
        return actions;
    }

    public void setActions(ArrayList<Object> actions) {
        this.actions = actions;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }
}
