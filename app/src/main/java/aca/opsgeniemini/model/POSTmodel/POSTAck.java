package aca.opsgeniemini.model.POSTmodel;

public class POSTAck {

    private String user;
    private String source;
    private String note;

    public POSTAck(String user,
                   String source,
                   String note) {
        this.user = user;
        this.source = source;
        this.note = note;
    }
}
