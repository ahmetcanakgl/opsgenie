package aca.opsgeniemini.model;

/**
 * Created by ahmetcanakgl on 28-May-18.
 */

public class FirebaseNotificationModel {

    private String alertId;
    private String alertMessage;

    public String getAlertId() {
        return alertId;
    }

    public void setAlertId(String alertId) {
        this.alertId = alertId;
    }

    public String getAlertMessage() {
        return alertMessage;
    }

    public void setAlertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
    }
}
