package aca.opsgeniemini.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import aca.opsgeniemini.model.submodel.Alert;

public class AlertDetailModel {

    @SerializedName("data")
    @Expose
    private Alert alert;
    @SerializedName("took")
    @Expose
    private String took;
    @SerializedName("requestId")
    @Expose
    private String requestId;

    public Alert getAlert() {
        return alert;
    }

    public void setAlert(Alert alert) {
        this.alert = alert;
    }

    public String getTook() {
        return took;
    }

    public void setTook(String took) {
        this.took = took;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
