package aca.opsgeniemini.web;

import java.util.HashMap;

import aca.opsgeniemini.model.AckModel;
import aca.opsgeniemini.model.AlertDetailModel;
import aca.opsgeniemini.model.AlertModel;
import aca.opsgeniemini.model.POSTmodel.POSTAck;
import retrofit2.Callback;

public class ApiCall {

    // Default queries must be here
    private HashMap<String, String> defaultQuery() {

        HashMap<String, String> filter = new HashMap<>();
        //filter.put("no default query", query);

        return filter;
    }

    public void getAlertsCall(int offset, Callback<AlertModel> alertModelCallback) {

        HashMap<String, String> queryStrings = new HashMap<>();
        queryStrings.put("limit", "20");
        queryStrings.put("offset", String.valueOf(offset));
        queryStrings.putAll(defaultQuery());

        RestInterface r = new ServiceConf().createService(RestInterface.class);
        r.getAlerts(queryStrings).enqueue(alertModelCallback);
    }

    public void getAlertDetailCall(String alertId, Callback<AlertDetailModel> alertDetCallback) {

        HashMap<String, String> queryStrings = new HashMap<>();
        queryStrings.put("identifierType", "id");
        queryStrings.putAll(defaultQuery());

        RestInterface r = new ServiceConf().createService(RestInterface.class);
        r.getAlertDetail(alertId, queryStrings).enqueue(alertDetCallback);
    }

    public void postAckCall(String alertId, POSTAck postAck, Callback<AckModel> ackModelCallback) {

        HashMap<String, String> queryStrings = new HashMap<>();
        queryStrings.put("identifierType", "id");
        queryStrings.putAll(defaultQuery());

        RestInterface r = new ServiceConf().createService(RestInterface.class);
        r.postAck(alertId, postAck, queryStrings).enqueue(ackModelCallback);
    }

}
