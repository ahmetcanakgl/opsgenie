package aca.opsgeniemini.web;

import java.util.Map;

import aca.opsgeniemini.model.AckModel;
import aca.opsgeniemini.model.AlertDetailModel;
import aca.opsgeniemini.model.AlertModel;
import aca.opsgeniemini.model.POSTmodel.POSTAck;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface RestInterface {

    @GET("/v2/alerts")
    Call<AlertModel> getAlerts(@QueryMap Map<String, String> queryStrings);

    @GET("/v2/alerts/{alert_id}")
    Call<AlertDetailModel> getAlertDetail(@Path(value = "alert_id", encoded = true) String alertId,
                                          @QueryMap Map<String, String> queryStrings);

    @POST("/v2/alerts/{alert_id}/acknowledge")
    Call<AckModel> postAck(@Path(value = "alert_id", encoded = true) String alertId,
                           @Body POSTAck postAck,
                           @QueryMap Map<String, String> queryStrings);
}